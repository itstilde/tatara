require 'chef'
require 'foodcritic'
require 'rspec/core/rake_task'
require 'rubocop/rake_task'
require 'bundler/setup'
task default: [:rubocop, :mock, :spec, :deploy]

desc 'Run all tasks'
task all: [
  :doc,
  :clean,
  :food,
  :rubocop,
  :mock,
  :spec,
  :kitchen,
  :deploy
]
desc 'Lists all the tasks.'
task :list do
  puts "Tasks: \n- #{Rake::Task.tasks.join("\n- ")}"
end

desc 'Creates README from doc/*md files & cookbook data'
task :doc do
Mixlib::ShellOut.new('bundle exec knife cookbook doc -t .readme.md.erb .').run_command
end

desc 'Pushes the cookbooks to my local vagrant boxes (or s3)'
task :deploy do
  system('./deploy_cookbooks.sh')
end

desc 'Creates mock JSON attr file for using in tests'
task :mock do
  require 'knife_cookbook_doc/attributes_model'
  require 'json'
  def flat_keys_to_nested(hash)
    hash.each_with_object({}) do |(key, value), all|
      key_parts = key.split('.').map!(&:to_sym)
      leaf = key_parts[0...-1].inject(all) { |acc, elem| acc[elem] ||= {} }
      leaf[key_parts.last] = value
    end
  end

  _metadata_cmd = Mixlib::ShellOut.new('knife cookbook metadata from file ./metadata.rb').run_command
  # puts _metadata_cmd.stdout
  _metadata = JSON.parse(::IO.read('./metadata.json'))
  attributes = {}
  Dir['./attributes/*.rb'].sort.each do |attribute_filename|
    model = KnifeCookbookDoc::AttributesModel.new(attribute_filename)
    model.attributes.each do |attr|
      short_name = attr[0].strip.gsub(/node\['/, '').gsub(/'\]\['/, '.').gsub(/'\]$/, '')
      # attributes[short_name] = attr[2].gsub(/#{@metadata['name']}/, '')
      attributes[short_name] = attr[2]
    end
  end
  attributes = flat_keys_to_nested attributes
  ::IO.write('test/attributes.json', attributes.to_json)
end

desc 'Beautifies up messy code you wrote'
task :clean do
  Mixlib::ShellOut.new("find . -name \"*.rb\" -exec ruby-beautify '{}' ';' > /dev/null 2>&1").run_command
  %w(Rakefile Gemfile Berksfile Thorfile).each do |clean_me|
    Mixlib::ShellOut.new("bundle exec ruby-beautify #{clean_me} && sed -i 's/ *$//' #{clean_me} > /dev/null 2>&1").run_command
  end
  # Removes trailing white spaces if there's any left over
  Mixlib::ShellOut.new("find . -name \"*.rb\" -exec sed -i 's/ *$//' '{}' ';' > /dev/null 2>&1").run_command
end

desc 'Run RSpec tests'
task :spec do
  RSpec::Core::RakeTask.new
end

desc 'Rubocop is the code style nazi you learn to love'
task :rubocop do
  RuboCop::RakeTask.new do |t|
    t.formatters = ['progress']
  end
end

desc 'Crituque your chef masterpiece with foodcritic'
task :food do
  FoodCritic::Rake::LintTask.new do |t|
    t.options = { fail_tags: ['all'] }
  end
end

desc 'Everything but the kitchen sink'
task :kitchen do
  begin
    require 'kitchen/rake_tasks'
    Kitchen::RakeTasks.new
  rescue LoadError
    puts '>>>>> Kitchen gem not loaded, omitting tasks.' unless ENV['CI']
  end
end
