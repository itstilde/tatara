##### Usage

Add this cookbook as a dependency in your `metadata.rb` file like this
```ruby
depends 'tatara'
```

or in your Berksfile like this
```ruby
# from bitbucket/github
cookbook 'tatara', git: 'git@bitbucket.org:jacob_mackenzie/tatara.git'
# or a local copy
cookbook 'tatara', path: '/path/to/tatara'
```

then install it with
```ruby
berks install
```

_Then you can include recipes in your runtime or in a wrapper cookbook:_
- `tatara::default` Does nothing!
- `tatara::nginx` Slowly sets up Nginx to run fast _TODO: stack json example here_
- `tatara::php` Sets up PHP, PHP-FPM and Composer & a few tools _TODO: stack json example here_
- `tatara::mysql` Sets up a mysql client and server if needed
- `tatara::crons`  Reconfigures cron jobs as per stack settings _TODO: stack json example here_

¯\\\_(ツ)\_/¯ Probably forgot something though
