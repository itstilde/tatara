##### Requirements

You will need [Ruby] with [Bundler]. Highly suggest you use RVM to install and
manage ruby version even though it breaks all the time

[VirtualBox] and [Vagrant] are required for integration testing with [Test Kitchen].

All you should need to do is:
```
bundle update
bundle install
rake all
```
