# *T.A.T.A.R.A*
TATARA (furnace) is the melting pot where it all comes together
###### _its~_
______

##### Usage

Add this cookbook as a dependency in your `metadata.rb` file like this
```ruby
depends 'tatara'
```

or in your Berksfile like this
```ruby
# from bitbucket/github
cookbook 'tatara', git: 'git@bitbucket.org:jacob_mackenzie/tatara.git'
# or a local copy
cookbook 'tatara', path: '/path/to/tatara'
```

then install it with
```ruby
berks install
```

_Then you can include recipes in your runtime or in a wrapper cookbook:_
- `tatara::default` Does nothing!
- `tatara::nginx` Slowly sets up Nginx to run fast _TODO: stack json example here_
- `tatara::php` Sets up PHP, PHP-FPM and Composer & a few tools _TODO: stack json example here_
- `tatara::mysql` Sets up a mysql client and server if needed
- `tatara::crons`  Reconfigures cron jobs as per stack settings _TODO: stack json example here_

¯\\\_(ツ)\_/¯ Probably forgot something though


##### TODO:

Loads of stuff


### Platform Support

* ubuntu (>= 16.04)

### Cookbook Dependencies

* tanto
* anbiru
* cron
* mysql
* chef_nginx
* php
* composer

### Attributes

Attribute | Description | Default | Choices
----------|-------------|---------|--------
`node['opsworks_initial_setup']['swapfile_instancetypes']` | `` | "[ ... ]" |
`node['nginx']['version']` | `` | "1.11.5" |
`node['nginx']['pagespeed']['version']` | `` | "1.11.33.4" |
`node['nginx']['install_method']` | `` | "source" |
`node['nginx']['user']` | `` | "www" |
`node['nginx']['group']` | `` | "www" |
`node['nginx']['source']['use_existing_user']` | `` | "true" |
`node['nginx']['source']['default_configure_flags']` | `` | "%W(" |
`node['nginx']['openssl_source']['version']` | `` | "1.0.2h" |
`node['nginx']['openssl_source']['url']` | `` | "http://www.openssl.org/source/openssl-\#{node['nginx']['openssl_source']['version']}.tar.gz" |
`node['nginx']['source']['modules']` | `` | "%w(" |
`node['php']['install_method']` | `` | "package" |
`node['php']['version']` | `` | "7.1" |
`node['php']['conf_dir']` | `` | "/etc/php/7.1/cli" |
`node['php']['fpm_user']` | `` | "www" |
`node['php']['fpm_group']` | `` | "www" |
`node['php']['fpm_listen_user']` | `` | "www" |
`node['php']['fpm_listen_group']` | `` | "www" |
`node['php']['ini']['template']` | `` | "php.ini.erb" |
`node['php']['ini']['cookbook']` | `` | "php" |
`node['php']['configure_options']` | `` | "%W(" |
`node['php']['packages']` | `` | "%w(php7.1-common php7.1-gd php7.1-readline php7.1-mysql php7.1-mbstring php7.1-fpm php7.1-xml php7.1 php7.1-cli php7.1-curl php7.1-mcrypt php7.1-json php7.1-opcache php7.1-bz2 php-pear)" |
`node['php']['mysql']['package']` | `` | "php7.1-mysql" |
`node['php']['curl']['package']` | `` | "php7.1-curl" |
`node['php']['apc']['package']` | `` | "php-apc" |
`node['php']['apcu']['package']` | `` | "php-apcu" |
`node['php']['gd']['package']` | `` | "php7.1-gd" |
`node['php']['ldap']['package']` | `` | "php7.1-ldap" |
`node['php']['pgsql']['package']` | `` | "php7.1-pgsql" |
`node['php']['sqlite']['package']` | `` | "php7.1-sqlite3" |
`node['php']['fpm_package']` | `` | "php7.1-fpm" |
`node['php']['fpm_pooldir']` | `` | "/etc/php/7.1/fpm/pool.d" |
`node['php']['fpm_service']` | `` | "php7.1-fpm" |
`node['php']['fpm_socket']` | `` | "/var/run/php/php7.1-fpm.sock" |
`node['php']['fpm_default_conf']` | `` | "/etc/php/7.1/fpm/pool.d/www.conf" |
`node['php']['ext_conf_dir']` | `` | "/etc/php/7.1/mods-available" |
`node['composer']['php_recipe']` | `` | "php::default" |
`node['composer']['global_configs']` | `` | "{ ... }" |

### Recipes

* tatara::default - Does nothing
* tatara::nginx - Sets up Nginx
* tatara::php - Sets up Composer & PHP
* tatara::mysql - Sets up mysql server and client
* tatara::crons - Reconfigures cronjobs

### Development and Testing

##### Requirements

You will need [Ruby] with [Bundler]. Highly suggest you use RVM to install and
manage ruby version even though it breaks all the time

[VirtualBox] and [Vagrant] are required for integration testing with [Test Kitchen].

All you should need to do is:
```
bundle update
bundle install
rake all
```


##### Rake Tasks:
- all        Run everything
- clean      Beautifies up messy code you wrote
- doc        Creates README & Other documentation files'
- deploy     Pushes the cookbooks to my local vagrant boxes & s3
- default    Runs rubocop, spec & deploy
- food       Crituque your chef masterpiece with foodcritic
- kitchen    Kitchen integration (broken atm)
- list       Lists this but not as good.
- rubocop    Rubocop is the code style nazi you learn to love
- spec       Rspec tests

##### Thor

Run `thor -T` to see all Thor tasks.

I still dunno how to really make use of this mighty hammer.


##### Contributing
Make a merge request or something, I dunno help me out!


##### License

MIT License

Copyright (c) 2016 its~

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


