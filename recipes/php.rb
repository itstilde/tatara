include_recipe 'anbiru::users' if Mixlib::ShellOut.new("getent passwd #{node['php']['fpm_user']}").run_command

if node['php']['install_method'] == 'source'
  %w(re2c libxml2-dev libcurl4-openssl-dev libjpeg-dev libpng-dev libxpm-dev libmysqlclient-dev libpq-dev libicu-dev libfreetype6-dev libldap2-dev libxslt-dev).each do |pkg|
    package pkg do
      action :install
    end
  end

  bash 'Set openssl thingys' do
    user 'root'
    code <<-EOH
  export OPENSSL_INCLUDE_DIR=#{Chef::Config['file_cache_path']}/openssl-#{node['nginx']['openssl_source']['version']}/include/openssl
       export DEP_OPENSSL_INCLUDE=#{Chef::Config['file_cache_path']}/openssl-#{node['nginx']['openssl_source']['version']}/include/openssl
    EOH
  end
end

include_recipe 'composer::default'

composer_install_global 'hirak/prestissimo' do
  version '^0.3'
  action :install
  ignore_failure true
end
