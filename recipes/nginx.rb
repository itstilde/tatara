require 'mixlib/shellout'

include_recipe 'anbiru::users' if Mixlib::ShellOut.new("getent passwd #{node['nginx']['user']}").run_command

# Install packages required to configure/compile nginx
%w(make libgoogle-perftools-dev libgoogle-perftools4 build-essential zlib1g-dev libpcre3-dev libgd-dev geoip-bin geoip-database libgeoip-dev).each do |pkg|
  package pkg do
    action :install
  end
end

# Create the directory we're nginx thinks the module is
directory "#{Chef::Config['file_cache_path']}/ngx_pagespeed" do
  action :create
  mode '0755'
  owner 'root'
  group 'root'
  not_if { ::File.directory?("#{Chef::Config['file_cache_path']}/ngx_pagespeed") }
end

# Name of file that we're downloading
ngx_pagespeed_name = "ngx_pagespeed-release-#{node['nginx']['pagespeed']['version']}-beta"

remote_file "#{Chef::Config['file_cache_path']}/#{ngx_pagespeed_name}.tar.gz" do
  group 'root'
  owner 'root'
  source "https://github.com/pagespeed/ngx_pagespeed/archive/v#{node['nginx']['pagespeed']['version']}-beta.tar.gz"
end

# Extract the files into the directory we just mde
bash "extract #{ngx_pagespeed_name}.tar.gz" do
  user 'root'
  cwd "#{Chef::Config['file_cache_path']}/"
  code <<-EOH
tar -xzf #{ngx_pagespeed_name}.tar.gz -C #{Chef::Config['file_cache_path']}/ngx_pagespeed/ --strip-components=1
  EOH
end

# Download Page Speed Library
remote_file "#{Chef::Config['file_cache_path']}/ngx_pagespeed/psol.tar.gz" do
  user 'root'
  source "https://dl.google.com/dl/page-speed/psol/#{node['nginx']['pagespeed']['version']}.tar.gz"
end

# Extract it
bash 'extract psol.tar.gz' do
  user 'root'
  cwd "#{Chef::Config['file_cache_path']}/ngx_pagespeed"
  code <<-EOH
tar -xzf psol.tar.gz
  EOH
end

external_nginx_modules = {
  'ngx_http_enhanced_memcached_module' => 'https://github.com/bpaquet/ngx_http_enhanced_memcached_module.git',
  'ngx_aws_auth' => 'https://github.com/anomalizer/ngx_aws_auth.git',
  'nchan' => 'https://github.com/slact/nchan.git'
}

external_nginx_modules.each do |nginx_module, repo|
  git "#{Chef::Config['file_cache_path']}/#{nginx_module}" do
    repository repo.to_s
    action :sync
    depth 1
    user 'root'
  end
end

directory '/var/ngx_pagespeed_cache' do
  mode '0755'
  owner node['nginx']['user']
  group node['nginx']['group']
  action :create
  not_if { ::File.directory?('/var/ngx_pagespeed_cache') }
end

include_recipe 'chef_nginx::default'
