#!/bin/bash
# TOODO
# Trap ctrl+C and clean up
#  copy cookbooks.tar.gz on s3 to and write a restore script to roll back
rm ./cookbooks.tar.gz > /dev/null 2>&1

#find . -name "*.rb" -exec ruby -c '{}' \; | grep -v 'Syntax OK'

#rake all
berks package ./cookbooks.tar.gz
echo " > Updating to Vagrant cookbooks..."
rm -rf /Users/jacob/katana/devops/vagrant/*/cookbooks
tar xf ./cookbooks.tar.gz -C /Users/jacob/katana/devops/vagrant/test_box/
tar xf ./cookbooks.tar.gz -C /Users/jacob/katana/devops/vagrant/opsworks-simulator/
