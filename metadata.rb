name 'tatara'
maintainer 'its~'
maintainer_email 'jacob@madeintatara.com'
license 'MIT'
description 'TATARA (furnace) is the melting pot where it all comes together'
long_description <<-EOH
TATARA (furnace) is the melting pot where it all comes together
    TODO: More documentation
EOH
version '0.1.0'
supports 'ubuntu', '>= 16.04'

issues_url 'https://jacob_mackenzie@bitbucket.org/jacob_mackenzie/tatara.git'
source_url 'https://jacob_mackenzie@bitbucket.org/jacob_mackenzie/tatara.git'

depends 'tanto' # includes tanto but not sure if this is needed here, should use Berksfile?
depends 'anbiru'
depends 'cron'
depends 'mysql'
depends 'chef_nginx'
depends 'php'
depends 'composer'

recipe 'tatara::default',           'Does nothing'
recipe 'tatara::nginx',             'Sets up Nginx'
recipe 'tatara::php',               'Sets up Composer & PHP'
recipe 'tatara::mysql',             'Sets up mysql server and client'
recipe 'tatara::crons',             'Reconfigures cronjobs'
