# Nginx
default['nginx']['version'] = '1.11.5'
default['nginx']['pagespeed']['version'] = '1.11.33.4'
default['nginx']['install_method'] = 'source'
default['nginx']['user'] = 'www'
default['nginx']['group'] = 'www'
default['nginx']['source']['use_existing_user'] = true

default['nginx']['source']['default_configure_flags'] = %W(
  --prefix=#{default['nginx']['source']['prefix']}
  --conf-path=#{default['nginx']['dir']}/nginx.conf
  --sbin-path=#{default['nginx']['source']['sbin_path']}
  --without-mail_pop3_module
  --without-mail_imap_module
  --without-mail_smtp_module
  --without-http_uwsgi_module
  --without-http_scgi_module
  --with-http_realip_module
  --with-http_stub_status_module
  --with-http_gzip_static_module
  --with-http_v2_module
  --with-file-aio
  --with-http_addition_module
  --with-http_image_filter_module
  --with-http_gunzip_module
  --with-pcre
  --with-pcre-jit
  --with-google_perftools_module
  --with-debug
  --with-http_image_filter_module=dynamic
  --with-http_geoip_module=dynamic
  --add-dynamic-module=#{Chef::Config['file_cache_path']}/ngx_pagespeed
  --add-dynamic-module=#{Chef::Config['file_cache_path']}/ngx_http_enhanced_memcached_module
  --add-dynamic-module=#{Chef::Config['file_cache_path']}/ngx_aws_auth
)
default['nginx']['openssl_source']['version'] = '1.0.2h'
default['nginx']['openssl_source']['url'] = "http://www.openssl.org/source/openssl-#{node['nginx']['openssl_source']['version']}.tar.gz"

default['nginx']['source']['modules'] = %w(
  chef_nginx::ngx_devel_module
  chef_nginx::http_ssl_module
  chef_nginx::ipv6
  chef_nginx::openssl_source
  chef_nginx::set_misc
  chef_nginx::headers_more_module
)

default['php']['install_method'] = 'package'
default['php']['version'] = '7.1'
default['php']['conf_dir'] = '/etc/php/7.1/cli'
default['php']['fpm_user'] = 'www'
default['php']['fpm_group'] = 'www'
default['php']['fpm_listen_user'] = 'www'
default['php']['fpm_listen_group'] = 'www'
default['php']['ini']['template'] = 'php.ini.erb'
default['php']['ini']['cookbook'] = 'php'
# Not needed but not getting rid of it lol
default['php']['configure_options'] = %W(
  --prefix=#{default['php']['prefix_dir']}
  --with-libdir=lib
  --with-config-file-path=#{default['php']['conf_dir']}
  --with-config-file-scan-dir=#{default['php']['ext_conf_dir']}
  --with-pear
  --enable-intl
  --enable-fpm
  --with-fpm-user=#{default['php']['fpm_user']}
  --with-fpm-group=#{default['php']['fpm_group']}
  --with-pdo-mysql
  --with-zlib
  --with-bz2
  --with-curl
  --enable-zip
  --with-kerberos
  --enable-exif
  --with-gd
  --enable-gd-native-ttf
  --with-gettext
  --with-gmp
  --with-mhash
  --with-iconv
  --with-imap
  --with-imap-ssl
  --enable-sockets
  --with-xmlrpc
  --with-mcrypt
  --enable-mbstring
  --disable-shared
  --with-openssl=#{Chef::Config['file_cache_path']}/openssl-#{default['nginx']['openssl_source']['version']}/include/openssl
  --with-openssl-dir=#{Chef::Config['file_cache_path']}/openssl-#{default['nginx']['openssl_source']['version']}/include/openssl
)
# Removed these - may need them back at some point tho
# --enable-soap
# --with-kerberos
# --with-mysql-sock
# --enable-ftp
# --with-mysqli=/usr/bin/mysql_config
# --with-mysql
# --with-t1lib
# --with-libevent-dir

default['php']['packages'] = %w(php7.1-common php7.1-gd php7.1-readline php7.1-mysql php7.1-mbstring php7.1-fpm php7.1-xml php7.1 php7.1-cli php7.1-curl php7.1-mcrypt php7.1-json php7.1-opcache php7.1-bz2 php-pear)
default['php']['mysql']['package'] = 'php7.1-mysql'
default['php']['curl']['package']  = 'php7.1-curl'
default['php']['apc']['package']   = 'php-apc'
default['php']['apcu']['package']  = 'php-apcu'
default['php']['gd']['package']    = 'php7.1-gd'
default['php']['ldap']['package']  = 'php7.1-ldap'
default['php']['pgsql']['package'] = 'php7.1-pgsql'
default['php']['sqlite']['package'] = 'php7.1-sqlite3'
default['php']['fpm_package']      = 'php7.1-fpm'
default['php']['fpm_pooldir']      = '/etc/php/7.1/fpm/pool.d'
default['php']['fpm_service']      = 'php7.1-fpm'
default['php']['fpm_socket']       = '/var/run/php/php7.1-fpm.sock'
default['php']['fpm_default_conf'] = '/etc/php/7.1/fpm/pool.d/www.conf'
default['php']['ext_conf_dir']     = '/etc/php/7.1/mods-available'
# So we install composer which first calls the php::default.rb and installs php
# TODO: Figure out a way here to skip including it if we've compiled and there's no updates or wotnot.
default['composer']['php_recipe'] = 'php::default'
default['composer']['global_configs'] = {
  'root' => { # TODO: Do this for katana and the other users too
    'github-oauth' => {
      'github.com' => 'a41fa7a34e64b669e1a4ac25687cd1d0f35ec4e3'
    }
  }
}
